### Data Analyst Challenge

The challenge was to find suspicious bank transactions based on bank and blockchain transaction data, the approach was to find pairs of bank and blockchain transactions that where of similar US dollar value and that occured within a couple of days of one another.

The criteria were motivated by the sample transaction: ![sample transaction](data/example-transaction.png "Example transaction")

The tools used where the Python Numpy and Pandas libraries, the exploration was done in a ![jupyter notebook](notebook.ipynb "Notebook").

<hr />

### Report

### Results
The analysis yielded 300 bank transactions with the same profile as the sample transaction.

### Approach

Motivated by the problem statement, available data and sample transactions the following metrics are used to find suspiciously similar pairs of blockchain and bank transactions:

- The time difference between a pair of transactions must be less than 2 days.
- The percentage USD amount difference between a pair of transactions must be less than 0.4.

The tools used are the Numpy and Pandas Python libraries.



```python
# Calculating diff: abs(bank_usd - blockchain_usd) * 100 / bank_usd
bank_usd = 1e6
blockchain_usd = 1003862.21061
abs(bank_usd - blockchain_usd) * 100 / bank_usd
```




    0.3862210609999951



### Loading datasets
Some bank transactions in the transactions map data set have missing data for the "begin_date" and "end_date" columns, these are dropped.


```python
import pandas as pd
import numpy as np
import random
from pathlib import Path
# Load bitcoin transactions
btc_dfs = []
for btc_data in (f"{year}.csv" for year in ('2015','2016','2017')):
    btc_dfs.append(pd.read_csv(Path('data', 'btc-large-transactions', btc_data),
                               index_col='time',
                               parse_dates=True,
                               dayfirst=True))
btc_df = pd.concat(btc_dfs)
# Load bank transactions
bc_df = pd.read_csv(Path('data', 'fincen','bank_connections.csv'))
tm_df = pd.read_csv(Path('data', 'fincen', 'transactions_map.csv'),
                                          parse_dates=['begin_date', 'end_date'],
                                          dayfirst=True)
tm_df = tm_df.dropna(axis=0)
```

### Compute difference in USD amount as a percentage using numpy broadcasting

- `A = tm_df['amount_transactions']` has length 4390
- `B = btc_df['Transaction_amount_USD']` has length 62357

$A$ and $B$ will be broadcast such that 
$A - B$ will yield a 4390 × 62537 matrix:

$
\begin{bmatrix} 
    \frac{A_1 - B_1}{A_1} & \frac{A_1 - B_2}{A_1} & \ldots & \frac{A_1 - B_{62357}}{A_1} \\
    \frac{A_2 - B_1}{A_2} & \frac{A_2 - B_2}{A_2} & \ldots & \frac{A_2 - B_{62357}}{A_2} \\
    \vdots &  & \ddots & \vdots \\
    \frac{A_{4390} - B_1}{A_{4390}} & \frac{A_{4390} - B_2}{A_{4390}} & \ldots & \frac{A_{4390} - B_{62357}}{A_{4390}}
 \end{bmatrix}
$

 So that each row corresponds to a bank transaction and the value in each column is its percentage difference from one of the 62537 blockchain transactions.
 
A gets broadcasted along its size 1 dimension (columns):
4390 × 62357

B gets broadcasted along its size 1 dimension (rows):
62357 × 62357


```python
bank_txns = tm_df['amount_transactions'].values
block_txns = btc_df['Transaction_amount_USD'].values
usd_diff = np.abs(bank_txns[:, None] - block_txns) / bank_txns[:, None]
```


```python
usd_diff.shape
```




    (4390, 62357)




```python
# Sanity check
for i in range(3):
    bank_idx = random.randint(0, len(bank_txns))
    block_idx = random.randint(0, len(block_txns))
    assert usd_diff[bank_idx, block_idx] == np.abs(bank_txns[bank_idx] - block_txns[block_idx]) / bank_txns[bank_idx]

```

### Determine row-wise minimum of difference matrix
The minimum value in each row is the smallest difference between the bank transaction and all blockchain transactions.
Therefore we seek the indices of all the differences below our threshold.


```python
threshold = 0.004
```


```python

usd_diff_5p = np.argwhere(usd_diff < threshold)
```


```python
usd_diff_5p.shape
```




    (316217, 2)



## Compute time (date) differences between bank and blockchain transactions 
Similarly to how the USD difference was calculated, the absolute difference between each bank transaction's date (using the begin_date field) and each blockchain transaction's date is determined using matrix arithmetic.



```python
time_diff = np.abs(tm_df['begin_date'].values[:, None] - btc_df.index.values)
```


```python
time_diff.shape
```




    (4390, 62357)



Determine pairs of indices corresponding to pairs of (bank, blockchain) transactions that occured within 2 days of each other


```python
date_threshold = 2 # days
```


```python
time_diff_2days = np.argwhere(time_diff < np.timedelta64(date_threshold, 'D'))
```

Determine the intersection of pairs in the set of filtered usd and time differences, these pairs will correspond to block & bank transactions that are:
- within threshold number of days of each other, and 
- within threshold percentage usd difference of each other.


```python
needles = set((tuple(x) for x in usd_diff_5p)).intersection(set(tuple(x) for x in time_diff_2days))
needles = np.array(list(needles))
```


```python
needles.shape
```




    (300, 2)



So there are 300 transaction pairs such that the time difference between each transaction is 2 days and the percentage usd difference in transaction value is less than 0.04



```python
bank_idxs = needles[:, 0]
block_idxs = needles[:, 1]
```

Collating and printing results of possibly suspicious bank transactions


```python
r = pd.DataFrame()
r['time'] = tm_df['begin_date'].iloc[bank_idxs]
r['originator_bank'] = tm_df['originator_bank'].iloc[bank_idxs]
r['filer_org_name'] = tm_df['filer_org_name'].iloc[bank_idxs]
r['beneficiary_bank'] = tm_df['beneficiary_bank'].iloc[bank_idxs]
r['bank_transaction_size_usd'] = tm_df['amount_transactions'].iloc[bank_idxs]
r['blockchain_transaction_size_usd'] = btc_df['Transaction_amount_USD'].iloc[block_idxs].values
r['diff'] = np.abs(r['bank_transaction_size_usd'] - r['blockchain_transaction_size_usd']) * 100 / r['blockchain_transaction_size_usd']
r['sender'] = btc_df['Sender'].iloc[block_idxs].values
r.index = r['time']
r = r.drop(columns=['time'])
```


```python
import pandas as pd
pd.options.display.float_format = '{:.2f}'.format
```


```python
r = r.sort_index()
r
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>originator_bank</th>
      <th>filer_org_name</th>
      <th>beneficiary_bank</th>
      <th>bank_transaction_size_usd</th>
      <th>blockchain_transaction_size_usd</th>
      <th>diff</th>
      <th>sender</th>
    </tr>
    <tr>
      <th>time</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2015-01-26</th>
      <td>DMS Bank &amp; Trust Ltd</td>
      <td>The Northern Trust Company</td>
      <td>Bank J Safra Sarasin Ltd</td>
      <td>1000000.00</td>
      <td>1000977.10</td>
      <td>0.10</td>
      <td>12U7EKBW6mPDkgiSmd7FiperFfEXfKV174</td>
    </tr>
    <tr>
      <th>2015-01-26</th>
      <td>DMS Bank &amp; Trust Ltd</td>
      <td>The Northern Trust Company</td>
      <td>Bank J Safra Sarasin Ltd</td>
      <td>1000000.00</td>
      <td>1001226.20</td>
      <td>0.12</td>
      <td>1DhPv4PZZRMUrtpchi8yjZSvqgSLwkBvUW</td>
    </tr>
    <tr>
      <th>2015-03-02</th>
      <td>Expobank</td>
      <td>The Bank of New York Mellon Corp.</td>
      <td>Credit Suisse AG</td>
      <td>1649757.00</td>
      <td>1649656.95</td>
      <td>0.01</td>
      <td>1L8TEn89MEnJVW6PVWQe6WjhjphuN7wgG4</td>
    </tr>
    <tr>
      <th>2015-03-02</th>
      <td>Expobank</td>
      <td>The Bank of New York Mellon Corp.</td>
      <td>Credit Suisse AG</td>
      <td>1649757.00</td>
      <td>1649657.58</td>
      <td>0.01</td>
      <td>1RG2Ghrrftaocs92KW65VpRkKdHRfZppc</td>
    </tr>
    <tr>
      <th>2015-03-02</th>
      <td>Expobank</td>
      <td>The Bank of New York Mellon Corp.</td>
      <td>Credit Suisse AG</td>
      <td>1649757.00</td>
      <td>1649658.77</td>
      <td>0.01</td>
      <td>1LGMtHxQhwWSfcSrAvBTFsL8Gt32rebDY2</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2017-01-10</th>
      <td>Gazprombank</td>
      <td>The Bank of New York Mellon Corp.</td>
      <td>Rigensis Bank AS</td>
      <td>2480000.00</td>
      <td>2482845.49</td>
      <td>0.11</td>
      <td>1JeTVrPZ8vn6gcEW3eNtVexreMMnsSDDWQ</td>
    </tr>
    <tr>
      <th>2017-01-10</th>
      <td>Gazprombank</td>
      <td>The Bank of New York Mellon Corp.</td>
      <td>Rigensis Bank AS</td>
      <td>2480000.00</td>
      <td>2488976.78</td>
      <td>0.36</td>
      <td>1Ehj37VdkyCXo84LmaJLjUadgMkgftmN2L</td>
    </tr>
    <tr>
      <th>2017-01-10</th>
      <td>Gazprombank</td>
      <td>The Bank of New York Mellon Corp.</td>
      <td>Rigensis Bank AS</td>
      <td>2480000.00</td>
      <td>2488399.39</td>
      <td>0.34</td>
      <td>14RL4nRzzqtuM4YWw1GQ2fK7yRUJPEUSc6</td>
    </tr>
    <tr>
      <th>2017-01-10</th>
      <td>Gazprombank</td>
      <td>The Bank of New York Mellon Corp.</td>
      <td>Rigensis Bank AS</td>
      <td>2480000.00</td>
      <td>2480254.41</td>
      <td>0.01</td>
      <td>1Hhepy2Jk2NBXyAtrTsfdaJ1L66J7jDQEV</td>
    </tr>
    <tr>
      <th>2017-04-20</th>
      <td>Berliner Volksbank Eg</td>
      <td>The Bank of New York Mellon Corp.</td>
      <td>Raiffeisen Bank International Ag</td>
      <td>2289240.00</td>
      <td>2288992.20</td>
      <td>0.01</td>
      <td>1BFhGnbmi9vDLF5izcER5M7z2BkWSXavZi</td>
    </tr>
  </tbody>
</table>
<p>300 rows × 7 columns</p>
</div>




```python
r.shape
```




    (300, 7)



The sample transaction is also included in the similar transactions.


```python
r.loc['2016-01-14']
```




    originator_bank                                                        Bank of China
    filer_org_name                                          China Investment Corporation
    beneficiary_bank                       Saigon Thuong Tin Commercial Joint Stock Bank
    bank_transaction_size_usd                                                 1000000.00
    blockchain_transaction_size_usd                                           1003862.21
    diff                                                                            0.38
    sender                             37A2u77XcFpmg4iaEuGGFVtEpupcXiFQei\n3M8wnd72ih...
    Name: 2016-01-14 00:00:00, dtype: object



### Further exploration
- Try unsupervised learning methods for anomaly detection, for instance k-means clustering or simply calculating multi-variate distance between relevant features(using the Mahalanobis distance metric for instance) where anomalies are discarded and tight clusters/groupings investigated further to determine if their similarity is suspicious.
- Use the bank_connections data set to investigate anomalous patterns of transactions between countries, which can be used in conjuction with the bank transactions data set to focus search on a subset of transactions.

## Descriptive stats


```python
r['originator_bank'].describe()
```




    count                          300
    unique                          44
    top       Zapad Banka Ad Podgorica
    freq                            93
    Name: originator_bank, dtype: object




```python
r['originator_bank'].value_counts()
```




    Zapad Banka Ad Podgorica                          93
    Vnesheconombank                                   45
    AS Expo Bank                                      32
    Hong Kong And Shanghai Banking Corporation Ltd    24
    Expobank                                          19
    Icici Bank Limited                                 9
    HSBC Hong Kong                                     9
    United Overseas Bank Limited                       6
    Bdo Unibank Inc                                    6
    Gazprombank Switzerland Ltd                        5
    Gazprombank                                        4
    Belarusky Narodny Bank                             4
    Falcon Private Bank Ltd                            4
    Skandinaviska Enskilda Banken                      3
    Societe Generale                                   2
    Bank of Communications                             2
    DMS Bank & Trust Ltd                               2
    AS Expobank                                        2
    Rigensis Bank AS                                   2
    Bank of China                                      2
    Bank Hapoalim B M                                  2
    Deutsche Bank AG Amsterdam                         1
    Pivdenny Bank                                      1
    SCSB Savings Department Branch                     1
    State Bank For Foreign Economic Aff                1
    National Australia Bank Limited                    1
    SEB Pank                                           1
    Versobank AS                                       1
    Volksbank Kamen Werne Eg                           1
    Trasta Komercbanka                                 1
    Ojsc Jscb International Financial Club             1
    Bank of Communications Co Ltd                      1
    Soyuz OAO                                          1
    Gorenjska Banka D.D., Kranj                        1
    Norvik Banka JSC                                   1
    Alfa-Bank                                          1
    Kleinwort Benson                                   1
    Berliner Volksbank Eg                              1
    Bank of Communications Co. Ltd                     1
    China Merchants Bank                               1
    Transkapitalbank                                   1
    Bnp Paribas Wealth Management                      1
    Garantibank International NV                       1
    Raiffeisen Zentralbankoesterreich AG               1
    Name: originator_bank, dtype: int64




```python
r['beneficiary_bank'].describe()
```




    count                      300
    unique                      37
    top       Ceska Sportelna A.S.
    freq                        93
    Name: beneficiary_bank, dtype: object




```python
r['beneficiary_bank'].value_counts()
```




    Ceska Sportelna A.S.                             93
    Baltikums Bank                                   45
    DBS Bank Ltd                                     40
    UniCredit Bank, Cjsc                             32
    Credit Suisse AG                                 20
    AS Meridian Trade Bank                           12
    Bank of India Indonesia                           9
    SCSB Savings Bank                                 6
    Regionala Investiciju Banka                       5
    JSC Norvik Banka                                  5
    Rigensis Bank AS                                  4
    ING Netherland NV                                 2
    Bank J Safra Sarasin Ltd                          2
    Jscb International Financial Club                 2
    China Minsheng Banking Corp                       1
    Kazkommertsbank                                   1
    Union Bancaire Privee                             1
    AS Expobank                                       1
    Standard Chartered Bank Hong Kong Ltd             1
    Kbl European Private Bankers S.A.                 1
    Raiffeisen Bank International Ag                  1
    VTB Bank                                          1
    Eurasian Development Bank                         1
    Norvik Banka, JSC                                 1
    EFG Bank                                          1
    United Overseas Bank Limited                      1
    Credit Suisse Switzerland Ltd                     1
    Shanghai Pudong Development Bank                  1
    New Moscow Bank                                   1
    Banque Pour Le Commerce Exterieur Lao Public      1
    Saigon Thuong Tin Commercial Joint Stock Bank     1
    Credit Suisse                                     1
    Ping An Bank Co Ltd                               1
    Macquarie Bank Limited                            1
    HSBC Hong Kong                                    1
    Euroclear Bank                                    1
    Branch Banking And Trust Company                  1
    Name: beneficiary_bank, dtype: int64




```python
r['bank_transaction_size_usd'].describe()
```




    count        300.00
    mean     2433699.69
    std      2410918.86
    min      1000000.00
    25%      1281000.00
    50%      1281000.00
    75%      2170000.00
    max     10000000.00
    Name: bank_transaction_size_usd, dtype: float64




```python
r['blockchain_transaction_size_usd'].describe()
```




    count        300.00
    mean     2433309.55
    std      2408690.49
    min      1000977.10
    25%      1278392.75
    50%      1285289.95
    75%      2174029.81
    max     10037478.11
    Name: blockchain_transaction_size_usd, dtype: float64




```python
r['diff'].describe()
```




    count   300.00
    mean      0.18
    std       0.11
    min       0.01
    25%       0.09
    50%       0.19
    75%       0.26
    max       0.39
    Name: diff, dtype: float64





